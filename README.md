# mybatis-do-po-mapping

## 介绍

基于mybatis的DO和PO之间的映射关系整理

通常PO的属性和数据库的字段是一一对应的，即PO的属性都是一些基本数据结构。
在这种情况下，当DO中的属性变得复杂后，Do与PO之间的映射也变得复杂。

综合来看，DO和PO之间的映射可概括为如下几种场景：

### 1. 简单映射

DO的属性相对简单，都是基础类型，和PO之间的属性能够一一对应。

[示例](src/main/java/cn/com/agree/dopomapping/directmapping)。

### 2. 值对象映射

通常DO中会包含一个甚至多个值对象，此时，值对象作为一个特殊的属性，无法直接映射到PO中，
因此有如下几种策略：

#### （1） 序列化

将DO的值对象属性序列化为json或者xml字符串后映射到PO的String字段。

[示例](src/main/java/cn/com/agree/dopomapping/valueobjectmapping/serializemapping)。

#### （2）平铺

将DO的值对象中包含的属性平铺映射到DO对应的PO中。需要注意解决属性名称冲突，和值对象嵌套问题。

[示例](src/main/java/cn/com/agree/dopomapping/valueobjectmapping/expandmapping)。

#### （3）单独PO

将DO的值对象属性单独映射为一个PO。此时该PO应当包含DO对应PO的主键信息，防止DO与值对象之间关系丢失。
同时也要考虑值对象嵌套问题。

[示例](src/main/java/cn/com/agree/dopomapping/valueobjectmapping/singletonpomapping)。

### 3. 继承映射

当DO之间存在共性时，会抽取公共部分提升为父类，如是便有了DO之间的继承关系。对于这种DO，在映射到PO时，有如下几种策略：

#### （1）类表继承

每一个DO均对应一个PO，具体来说就是每个子类均对应一个PO，储存公共属性的父类DO也对应一个PO。
此时，子类PO的主键应当与其对应的父类主键一致.

[示例](src/main/java/cn/com/agree/dopomapping/Inheritmapping/classtablemapping)。

#### （2）单表继承
所有的DO映射到同一个PO，PO中对应所有的DO属性，PO通过一个类型字段来区分不同DO的数据。

[示例](src/main/java/cn/com/agree/dopomapping/Inheritmapping/singletontablemapping)。

#### （3）多表继承
每个子类DO均对应一个PO，PO中包括子类DO的独立属性和父类DO的公共属性。所有子类的主键唯一，不能重复。

[示例](src/main/java/cn/com/agree/dopomapping/Inheritmapping/multitablemapping)。
