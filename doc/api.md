---
title: http v1.0.0
language_tabs:
  - shell: Shell
  - http: HTTP
  - javascript: JavaScript
  - ruby: Ruby
  - python: Python
  - php: PHP
  - java: Java
  - go: Go
toc_footers: []
includes: []
search: true
code_clipboard: true
highlight_theme: darkula
headingLevel: 2
generator: "@tarslib/widdershins v4.0.17"

---

# http

> v1.0.0

Base URLs:

* <a href="http://localhost:8080">开发环境: http://localhost:8080</a>

# Authentication

# 直接映射

## PUT 添加用户

PUT /users

> Body 请求参数

```json
{
  "name": "string",
  "age": 0
}
```

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|body|body|[UserDTO](#schemauserdto)| 否 |none|

> 返回示例

> 200 Response

```json
{}
```

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## GET 查询用户信息

GET /users/{id}

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|id|path|string| 是 |none|

> 返回示例

> 200 Response

```json
{}
```

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

# 值对象映射/序列化

## PUT 添加学生

PUT /students

> Body 请求参数

```json
{
  "id": "string",
  "name": "string",
  "age": 0,
  "courses": [
    {
      "courseCode": "string",
      "courseName": "string",
      "creditHours": 0,
      "instructor": "string"
    }
  ]
}
```

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|body|body|[StudentDTO](#schemastudentdto)| 否 |none|

> 返回示例

> 200 Response

```json
{}
```

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## GET 查询学生信息

GET /students/{id}

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|id|path|string| 是 |none|

> 返回示例

> 200 Response

```json
{}
```

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

# 值对象映射/平铺

## PUT 添加汽车

PUT /cars

> Body 请求参数

```json
{
  "id": "string",
  "make": "string",
  "model": "string",
  "year": 0,
  "color": "string",
  "price": 0,
  "engine": {
    "type": "string",
    "horsepower": 0
  }
}
```

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|body|body|[CarDTO](#schemacardto)| 否 |none|

> 返回示例

> 200 Response

```json
{}
```

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## GET 获取车辆信息

GET /cars/{id}

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|id|path|string| 是 |none|

> 返回示例

> 200 Response

```json
{}
```

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

# 值对象映射/单独PO

## PUT 添加客户信息

PUT /customers

> Body 请求参数

```json
{
  "id": 0,
  "name": "string",
  "age": 0,
  "address": {
    "province": "string",
    "city": "string",
    "county": "string",
    "street": "string"
  }
}
```

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|body|body|[CustomerDTO](#schemacustomerdto)| 否 |none|

> 返回示例

> 200 Response

```json
{}
```

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## GET 查询客户信息

GET /customers/{id}

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|id|path|string| 是 |none|

> 返回示例

> 200 Response

```json
{}
```

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

# 继承映射/单表映射

## PUT 添加订单-实体订单

PUT /orders

> Body 请求参数

```json
{
  "id": "string",
  "orderId": "string",
  "amount": 0,
  "customerName": "string",
  "date": "string",
  "orderType": "string",
  "storeLocation": "string",
  "salespersonName": "string",
  "isMembershipDiscount": true,
  "paymentMethod": "string",
  "shippingAddress": "string",
  "isGift": true
}
```

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|body|body|[OrderDTO](#schemaorderdto)| 否 |none|

> 返回示例

> 200 Response

```json
{}
```

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## GET 查询订单

GET /orders/{id}

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|id|path|string| 是 |或者2|

> 返回示例

> 200 Response

```json
{}
```

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

# 继承映射/多表继承

## PUT 添加职工-高管

PUT /employees

> Body 请求参数

```json
{
  "id": "string",
  "employeeId": "string",
  "fullName": "string",
  "age": 0,
  "department": "string",
  "salary": 0,
  "employeeType": "string",
  "teamSize": 0,
  "officeLocation": "string",
  "hasStockOptions": true,
  "supervisorName": "string",
  "isFullTime": true,
  "workstation": "string"
}
```

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|body|body|[EmployeeDTO](#schemaemployeedto)| 否 |none|

> 返回示例

> 200 Response

```json
{}
```

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## GET 查询职工信息

GET /employees/{id}

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|id|path|string| 是 |或者 1738123863737880577|

> 返回示例

> 200 Response

```json
{}
```

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

# 继承映射/类表继承

## PUT 添加动物-猫

PUT /animals

> Body 请求参数

```json
{
  "id": "string",
  "species": "string",
  "age": 0,
  "color": "string",
  "weight": 0,
  "type": "string",
  "isFluffy": true,
  "isOutdoor": true,
  "breed": "string",
  "isTrained": true
}
```

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|body|body|[AnimalDTO](#schemaanimaldto)| 否 |none|

> 返回示例

> 200 Response

```json
{}
```

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## GET 查询动物信息

GET /animals/{id}

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|id|path|string| 是 |或者 1738128028874625026|

> 返回示例

> 200 Response

```json
{}
```

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

# 数据模型

<h2 id="tocS_AddressDTO">AddressDTO</h2>

<a id="schemaaddressdto"></a>
<a id="schema_AddressDTO"></a>
<a id="tocSaddressdto"></a>
<a id="tocsaddressdto"></a>

```json
{
  "province": "string",
  "city": "string",
  "county": "string",
  "street": "string"
}

```

### 属性

|名称|类型|必选|约束|中文名|说明|
|---|---|---|---|---|---|
|province|string|true|none||none|
|city|string|true|none||none|
|county|string|true|none||none|
|street|string|true|none||none|

<h2 id="tocS_EngineDTO">EngineDTO</h2>

<a id="schemaenginedto"></a>
<a id="schema_EngineDTO"></a>
<a id="tocSenginedto"></a>
<a id="tocsenginedto"></a>

```json
{
  "type": "string",
  "horsepower": 0
}

```

### 属性

|名称|类型|必选|约束|中文名|说明|
|---|---|---|---|---|---|
|type|string|true|none||none|
|horsepower|integer|true|none||none|

<h2 id="tocS_CourseDTO">CourseDTO</h2>

<a id="schemacoursedto"></a>
<a id="schema_CourseDTO"></a>
<a id="tocScoursedto"></a>
<a id="tocscoursedto"></a>

```json
{
  "courseCode": "string",
  "courseName": "string",
  "creditHours": 0,
  "instructor": "string"
}

```

### 属性

|名称|类型|必选|约束|中文名|说明|
|---|---|---|---|---|---|
|courseCode|string|true|none||none|
|courseName|string|true|none||none|
|creditHours|integer|true|none||none|
|instructor|string|true|none||none|

<h2 id="tocS_AnimalDTO">AnimalDTO</h2>

<a id="schemaanimaldto"></a>
<a id="schema_AnimalDTO"></a>
<a id="tocSanimaldto"></a>
<a id="tocsanimaldto"></a>

```json
{
  "id": "string",
  "species": "string",
  "age": 0,
  "color": "string",
  "weight": 0,
  "type": "string",
  "isFluffy": true,
  "isOutdoor": true,
  "breed": "string",
  "isTrained": true
}

```

### 属性

|名称|类型|必选|约束|中文名|说明|
|---|---|---|---|---|---|
|id|string|false|none||none|
|species|string|true|none||none|
|age|integer|true|none||none|
|color|string|true|none||none|
|weight|number|true|none||none|
|type|string|true|none||none|
|isFluffy|boolean|false|none||none|
|isOutdoor|boolean|false|none||none|
|breed|string|false|none||none|
|isTrained|boolean|false|none||none|

<h2 id="tocS_EmployeeDTO">EmployeeDTO</h2>

<a id="schemaemployeedto"></a>
<a id="schema_EmployeeDTO"></a>
<a id="tocSemployeedto"></a>
<a id="tocsemployeedto"></a>

```json
{
  "id": "string",
  "employeeId": "string",
  "fullName": "string",
  "age": 0,
  "department": "string",
  "salary": 0,
  "employeeType": "string",
  "teamSize": 0,
  "officeLocation": "string",
  "hasStockOptions": true,
  "supervisorName": "string",
  "isFullTime": true,
  "workstation": "string"
}

```

### 属性

|名称|类型|必选|约束|中文名|说明|
|---|---|---|---|---|---|
|id|string|false|none||none|
|employeeId|string|true|none||none|
|fullName|string|true|none||none|
|age|integer|true|none||none|
|department|string|true|none||none|
|salary|number|true|none||none|
|employeeType|string|true|none||none|
|teamSize|integer|false|none||none|
|officeLocation|string|false|none||none|
|hasStockOptions|boolean|false|none||none|
|supervisorName|string|false|none||none|
|isFullTime|boolean|false|none||none|
|workstation|string|false|none||none|

<h2 id="tocS_OrderDTO">OrderDTO</h2>

<a id="schemaorderdto"></a>
<a id="schema_OrderDTO"></a>
<a id="tocSorderdto"></a>
<a id="tocsorderdto"></a>

```json
{
  "id": "string",
  "orderId": "string",
  "amount": 0,
  "customerName": "string",
  "date": "string",
  "orderType": "string",
  "storeLocation": "string",
  "salespersonName": "string",
  "isMembershipDiscount": true,
  "paymentMethod": "string",
  "shippingAddress": "string",
  "isGift": true
}

```

### 属性

|名称|类型|必选|约束|中文名|说明|
|---|---|---|---|---|---|
|id|string|false|none||none|
|orderId|string|true|none||none|
|amount|number|true|none||none|
|customerName|string|true|none||none|
|date|string|true|none||none|
|orderType|string|true|none||none|
|storeLocation|string|false|none||none|
|salespersonName|string|false|none||none|
|isMembershipDiscount|boolean|false|none||none|
|paymentMethod|string|false|none||none|
|shippingAddress|string|false|none||none|
|isGift|boolean|false|none||none|

<h2 id="tocS_CustomerDTO">CustomerDTO</h2>

<a id="schemacustomerdto"></a>
<a id="schema_CustomerDTO"></a>
<a id="tocScustomerdto"></a>
<a id="tocscustomerdto"></a>

```json
{
  "id": 0,
  "name": "string",
  "age": 0,
  "address": {
    "province": "string",
    "city": "string",
    "county": "string",
    "street": "string"
  }
}

```

### 属性

|名称|类型|必选|约束|中文名|说明|
|---|---|---|---|---|---|
|id|integer|false|none||none|
|name|string|true|none||none|
|age|integer|true|none||none|
|address|[AddressDTO](#schemaaddressdto)|true|none||none|

<h2 id="tocS_CarDTO">CarDTO</h2>

<a id="schemacardto"></a>
<a id="schema_CarDTO"></a>
<a id="tocScardto"></a>
<a id="tocscardto"></a>

```json
{
  "id": "string",
  "make": "string",
  "model": "string",
  "year": 0,
  "color": "string",
  "price": 0,
  "engine": {
    "type": "string",
    "horsepower": 0
  }
}

```

### 属性

|名称|类型|必选|约束|中文名|说明|
|---|---|---|---|---|---|
|id|string|false|none||none|
|make|string|true|none||none|
|model|string|true|none||none|
|year|integer|true|none||none|
|color|string|true|none||none|
|price|number|true|none||none|
|engine|[EngineDTO](#schemaenginedto)|true|none||none|

<h2 id="tocS_StudentDTO">StudentDTO</h2>

<a id="schemastudentdto"></a>
<a id="schema_StudentDTO"></a>
<a id="tocSstudentdto"></a>
<a id="tocsstudentdto"></a>

```json
{
  "id": "string",
  "name": "string",
  "age": 0,
  "courses": [
    {
      "courseCode": "string",
      "courseName": "string",
      "creditHours": 0,
      "instructor": "string"
    }
  ]
}

```

### 属性

|名称|类型|必选|约束|中文名|说明|
|---|---|---|---|---|---|
|id|string|false|none||none|
|name|string|true|none||none|
|age|integer|true|none||none|
|courses|[[CourseDTO](#schemacoursedto)]|true|none||none|

<h2 id="tocS_UserDTO">UserDTO</h2>

<a id="schemauserdto"></a>
<a id="schema_UserDTO"></a>
<a id="tocSuserdto"></a>
<a id="tocsuserdto"></a>

```json
{
  "name": "string",
  "age": 0
}

```

### 属性

|名称|类型|必选|约束|中文名|说明|
|---|---|---|---|---|---|
|name|string|true|none||none|
|age|integer|true|none||none|

