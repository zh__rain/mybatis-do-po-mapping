CREATE TABLE tb_user (
                            id BIGINT AUTO_INCREMENT PRIMARY KEY,
                            name VARCHAR(255),
                            age INT
);

CREATE TABLE tb_animal (
                            id VARCHAR(255) PRIMARY KEY,
                            species VARCHAR(255),
                            age INT,
                            color VARCHAR(255),
                            weight DOUBLE,
                            type VARCHAR(255)
);

CREATE TABLE tb_cat (
                            id VARCHAR(255) NOT NULL,
                            is_fluffy BOOLEAN,
                            is_outdoor BOOLEAN,
                            PRIMARY KEY (id)
);

CREATE TABLE tb_dog (
                            id VARCHAR(255) NOT NULL,
                            breed VARCHAR(255),
                            is_trained BOOLEAN,
                            PRIMARY KEY (id)
);

CREATE TABLE tb_manager (
                            id VARCHAR(255) NOT NULL,
                            employee_id VARCHAR(255),
                            full_name VARCHAR(255),
                            age INT,
                            department VARCHAR(255),
                            salary DOUBLE,
                            team_size INT,
                            office_location VARCHAR(255),
                            has_stock_options BOOLEAN,
                            PRIMARY KEY (id)
);

CREATE TABLE tb_staff (
                            id VARCHAR(255) NOT NULL,
                            employee_id VARCHAR(255),
                            full_name VARCHAR(255),
                            age INT,
                            department VARCHAR(255),
                            salary DOUBLE,
                            supervisor_name VARCHAR(255),
                            is_full_time BOOLEAN,
                            workstation VARCHAR(255),
                            PRIMARY KEY (id)
);

CREATE TABLE tb_order (
                          id BIGINT AUTO_INCREMENT NOT NULL,
                          order_id VARCHAR(255),
                          amount DOUBLE,
                          customer_name VARCHAR(255),
                          date VARCHAR(255),
                          order_type VARCHAR(255),
                          payment_method VARCHAR(255),
                          shipping_address VARCHAR(255),
                          is_gift BOOLEAN,
                          store_location VARCHAR(255),
                          salesperson_name VARCHAR(255),
                          is_membership_discount BOOLEAN,
                          PRIMARY KEY (id)
);

CREATE TABLE tb_car (
                        id VARCHAR(255) NOT NULL,
                        make VARCHAR(255),
                        model VARCHAR(255),
                        year INT,
                        color VARCHAR(255),
                        price DOUBLE,
                        type VARCHAR(255),
                        horsepower INT,
                        PRIMARY KEY (id)
);

CREATE TABLE tb_student (
                            id BIGINT AUTO_INCREMENT NOT NULL,
                            name VARCHAR(255),
                            age INT,
                            courses TEXT,
                            PRIMARY KEY (id)
);

CREATE TABLE tb_address (
                            id VARCHAR(255) NOT NULL,
                            customer_id VARCHAR(255),
                            province VARCHAR(255),
                            city VARCHAR(255),
                            county VARCHAR(255),
                            street VARCHAR(255),
                            PRIMARY KEY (id)
);

CREATE TABLE tb_customer (
                             id VARCHAR(255) NOT NULL,
                             name VARCHAR(255),
                             age INT,
                             PRIMARY KEY (id)
);