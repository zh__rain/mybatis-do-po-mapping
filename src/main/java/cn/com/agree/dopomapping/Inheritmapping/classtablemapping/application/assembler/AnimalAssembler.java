package cn.com.agree.dopomapping.Inheritmapping.classtablemapping.application.assembler;

import cn.com.agree.dopomapping.Inheritmapping.classtablemapping.application.dto.AnimalDTO;
import cn.com.agree.dopomapping.Inheritmapping.classtablemapping.domain.AnimalDO;
import cn.com.agree.dopomapping.Inheritmapping.classtablemapping.domain.CatDO;
import cn.com.agree.dopomapping.Inheritmapping.classtablemapping.domain.DogDO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class AnimalAssembler {

    @Autowired
    private CatAssembler catAssembler;

    @Autowired
    private DogAssembler dogAssembler;

    public AnimalDTO toDTO(AnimalDO animalDO) {
        if (animalDO instanceof CatDO) {
            return catAssembler.toDTO((CatDO) animalDO);
        } else if (animalDO instanceof DogDO) {
            return dogAssembler.toDTO((DogDO) animalDO);
        }
        return null;
    }

    public AnimalDO toEntity(AnimalDTO animalDTO) {
        switch (animalDTO.getType()) {
            case CAT:
                return catAssembler.toEntity(animalDTO);
            case DOG:
                return dogAssembler.toEntity(animalDTO);
        }
        return null;
    }

}
