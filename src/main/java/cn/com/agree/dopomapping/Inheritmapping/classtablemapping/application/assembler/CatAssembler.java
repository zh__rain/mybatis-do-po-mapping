package cn.com.agree.dopomapping.Inheritmapping.classtablemapping.application.assembler;

import cn.com.agree.dopomapping.Inheritmapping.classtablemapping.application.dto.AnimalDTO;
import cn.com.agree.dopomapping.Inheritmapping.classtablemapping.domain.CatDO;
import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;

@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public interface CatAssembler {

    CatDO toEntity(AnimalDTO catDTO);

    AnimalDTO toDTO(CatDO catDO);

}
