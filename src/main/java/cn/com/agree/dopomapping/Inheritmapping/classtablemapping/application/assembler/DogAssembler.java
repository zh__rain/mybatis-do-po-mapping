package cn.com.agree.dopomapping.Inheritmapping.classtablemapping.application.assembler;

import cn.com.agree.dopomapping.Inheritmapping.classtablemapping.application.dto.AnimalDTO;
import cn.com.agree.dopomapping.Inheritmapping.classtablemapping.domain.DogDO;
import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;

@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public interface DogAssembler {

    DogDO toEntity(AnimalDTO dogDTO);

    AnimalDTO toDTO(DogDO dogDO);

}
