package cn.com.agree.dopomapping.Inheritmapping.classtablemapping.application.dto;

import cn.com.agree.dopomapping.Inheritmapping.classtablemapping.domain.valueobj.AnimalTypeEnum;
import lombok.Data;

@Data
public class AnimalDTO {

    private String id;        // 主键

    private String species;   // 动物的种类

    private int age;          // 动物的年龄

    private String color;     // 动物的颜色

    private double weight;    // 动物的体重

    private AnimalTypeEnum type;    // 动物类型


    private boolean isFluffy;      // 猫是否有毛

    private boolean isOutdoor;     // 猫是否喜欢户外活动



    private String breed;          // 狗的品种

    private boolean isTrained;     // 狗是否经过训练


}
