package cn.com.agree.dopomapping.Inheritmapping.classtablemapping.application.service;

import cn.com.agree.dopomapping.Inheritmapping.classtablemapping.application.assembler.AnimalAssembler;
import cn.com.agree.dopomapping.Inheritmapping.classtablemapping.application.dto.AnimalDTO;
import cn.com.agree.dopomapping.Inheritmapping.classtablemapping.domain.AnimalDO;
import cn.com.agree.dopomapping.Inheritmapping.classtablemapping.domain.repository.AnimalRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AnimalService {

    @Autowired
    private AnimalAssembler animalAssembler;

    @Autowired
    private AnimalRepository animalRepository;


    public AnimalDTO findAnimal(String id) {
        return animalAssembler.toDTO(animalRepository.findById(id));
    }

    public AnimalDTO saveAnimal(AnimalDTO animalDTO) {
        AnimalDO animalDO = animalAssembler.toEntity(animalDTO);
        animalDO = animalRepository.save(animalDO);
        return animalAssembler.toDTO(animalDO);
    }
}
