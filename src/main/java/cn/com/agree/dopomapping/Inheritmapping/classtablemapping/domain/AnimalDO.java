package cn.com.agree.dopomapping.Inheritmapping.classtablemapping.domain;

import cn.com.agree.dopomapping.Inheritmapping.classtablemapping.domain.valueobj.AnimalTypeEnum;
import lombok.Data;

@Data
public class AnimalDO {

    private String id;        // 主键

    private String species;   // 动物的种类

    private int age;          // 动物的年龄

    private String color;     // 动物的颜色

    private double weight;    // 动物的体重

    private AnimalTypeEnum type;    // 动物类型

}
