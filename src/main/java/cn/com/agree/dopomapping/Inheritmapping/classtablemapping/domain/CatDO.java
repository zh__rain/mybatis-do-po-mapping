package cn.com.agree.dopomapping.Inheritmapping.classtablemapping.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class CatDO extends AnimalDO {

    private boolean isFluffy;      // 猫是否有毛

    private boolean isOutdoor;     // 猫是否喜欢户外活动

}
