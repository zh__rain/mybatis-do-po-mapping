package cn.com.agree.dopomapping.Inheritmapping.classtablemapping.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class DogDO extends AnimalDO {

    private String breed;          // 狗的品种

    private boolean isTrained;     // 狗是否经过训练

}
