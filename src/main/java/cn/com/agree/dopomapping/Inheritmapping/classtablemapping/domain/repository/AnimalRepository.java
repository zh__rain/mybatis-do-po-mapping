package cn.com.agree.dopomapping.Inheritmapping.classtablemapping.domain.repository;

import cn.com.agree.dopomapping.Inheritmapping.classtablemapping.domain.AnimalDO;

public interface AnimalRepository {

    AnimalDO findById(String id);

    AnimalDO save(AnimalDO animalDO);

}
