package cn.com.agree.dopomapping.Inheritmapping.classtablemapping.infrastructure.converter;

import cn.com.agree.dopomapping.Inheritmapping.classtablemapping.domain.AnimalDO;
import cn.com.agree.dopomapping.Inheritmapping.classtablemapping.infrastructure.po.AnimalPO;
import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;

@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public interface AnimalConverter {

    AnimalPO toPO(AnimalDO customer);

    AnimalDO toEntity(AnimalPO customerPO);

}
