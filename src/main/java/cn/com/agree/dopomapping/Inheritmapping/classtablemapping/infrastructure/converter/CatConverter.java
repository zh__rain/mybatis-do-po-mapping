package cn.com.agree.dopomapping.Inheritmapping.classtablemapping.infrastructure.converter;

import cn.com.agree.dopomapping.Inheritmapping.classtablemapping.domain.CatDO;
import cn.com.agree.dopomapping.Inheritmapping.classtablemapping.infrastructure.po.AnimalPO;
import cn.com.agree.dopomapping.Inheritmapping.classtablemapping.infrastructure.po.CatPO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingConstants;

@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public interface CatConverter {



//    // 错误 ???? 位置规划?
//    CustomerPO toPO(IndividualCustomer individualCustomer);

    // NOTE: 注意映射关系的对应,父类与子类关联关系的赋值 => do子类的id 是 po父类的id，同时也是 po子类的customerid
    // ?? 在save的时候，是否有必要设定 id？以及测试效果？
    CatPO toPO(CatDO catDO);

    // NOTE: 注意映射关系的对应,父类与子类关联关系的赋值 => po父类的id 是 do子类的id
    @Mapping(target ="id", source = "animalPO.id")
    CatDO toDO(AnimalPO animalPO, CatPO catPO);

}
