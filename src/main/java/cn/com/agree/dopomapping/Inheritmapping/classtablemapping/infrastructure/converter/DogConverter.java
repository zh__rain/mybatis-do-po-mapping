package cn.com.agree.dopomapping.Inheritmapping.classtablemapping.infrastructure.converter;

import cn.com.agree.dopomapping.Inheritmapping.classtablemapping.domain.DogDO;
import cn.com.agree.dopomapping.Inheritmapping.classtablemapping.infrastructure.po.AnimalPO;
import cn.com.agree.dopomapping.Inheritmapping.classtablemapping.infrastructure.po.DogPO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingConstants;

@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public interface DogConverter {

    DogPO toPO(DogDO dogDO);

    @Mapping(target ="id", source = "animalPO.id")
    DogDO toDO(AnimalPO animalPO, DogPO dogPO);

}
