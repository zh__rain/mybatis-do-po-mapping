package cn.com.agree.dopomapping.Inheritmapping.classtablemapping.infrastructure.mapper;

import cn.com.agree.dopomapping.Inheritmapping.classtablemapping.infrastructure.po.AnimalPO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface AnimalPOMapper extends BaseMapper<AnimalPO> {
}
