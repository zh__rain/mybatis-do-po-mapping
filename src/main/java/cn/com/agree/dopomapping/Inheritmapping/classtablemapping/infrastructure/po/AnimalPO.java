package cn.com.agree.dopomapping.Inheritmapping.classtablemapping.infrastructure.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName("tb_animal")
public class AnimalPO {

    @TableId(type = IdType.ASSIGN_ID)
    private String id;        // 主键

    private String species;   // 动物的种类

    private int age;          // 动物的年龄

    private String color;     // 动物的颜色

    private double weight;    // 动物的体重

    private String type;    //动物类别

}
