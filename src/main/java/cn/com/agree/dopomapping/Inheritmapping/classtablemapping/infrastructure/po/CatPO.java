package cn.com.agree.dopomapping.Inheritmapping.classtablemapping.infrastructure.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@TableName("tb_cat")
@Data
public class CatPO {

    @TableId(type = IdType.INPUT)
    private String id;

    @TableField("is_fluffy")
    private boolean isFluffy;      // 猫是否有毛

    @TableField("is_outdoor")
    private boolean isOutdoor;     // 猫是否喜欢户外活动

}
