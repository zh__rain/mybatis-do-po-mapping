package cn.com.agree.dopomapping.Inheritmapping.classtablemapping.infrastructure.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName("tb_dog")
public class DogPO {

    @TableId(type = IdType.INPUT)
    private String id;

    private String breed;          // 狗的品种

    @TableField("is_trained")
    private boolean isTrained;     // 狗是否经过训练

}
