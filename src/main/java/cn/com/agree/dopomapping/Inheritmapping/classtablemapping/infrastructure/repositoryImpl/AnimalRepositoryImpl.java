package cn.com.agree.dopomapping.Inheritmapping.classtablemapping.infrastructure.repositoryImpl;

import cn.com.agree.dopomapping.Inheritmapping.classtablemapping.domain.AnimalDO;
import cn.com.agree.dopomapping.Inheritmapping.classtablemapping.domain.CatDO;
import cn.com.agree.dopomapping.Inheritmapping.classtablemapping.domain.DogDO;
import cn.com.agree.dopomapping.Inheritmapping.classtablemapping.domain.repository.AnimalRepository;
import cn.com.agree.dopomapping.Inheritmapping.classtablemapping.domain.valueobj.AnimalTypeEnum;
import cn.com.agree.dopomapping.Inheritmapping.classtablemapping.infrastructure.converter.AnimalConverter;
import cn.com.agree.dopomapping.Inheritmapping.classtablemapping.infrastructure.converter.CatConverter;
import cn.com.agree.dopomapping.Inheritmapping.classtablemapping.infrastructure.converter.DogConverter;
import cn.com.agree.dopomapping.Inheritmapping.classtablemapping.infrastructure.mapper.AnimalPOMapper;
import cn.com.agree.dopomapping.Inheritmapping.classtablemapping.infrastructure.mapper.CatPOMapper;
import cn.com.agree.dopomapping.Inheritmapping.classtablemapping.infrastructure.mapper.DogPOMapper;
import cn.com.agree.dopomapping.Inheritmapping.classtablemapping.infrastructure.po.AnimalPO;
import cn.com.agree.dopomapping.Inheritmapping.classtablemapping.infrastructure.po.CatPO;
import cn.com.agree.dopomapping.Inheritmapping.classtablemapping.infrastructure.po.DogPO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public class AnimalRepositoryImpl implements AnimalRepository {

    @Autowired
    private AnimalPOMapper animalPOMapper;

    @Autowired
    private AnimalConverter animalConverter;

    @Autowired
    private CatPOMapper catPOMapper;

    @Autowired
    private CatConverter catConverter;

    @Autowired
    private DogPOMapper dogPOMapper;

    @Autowired
    private DogConverter dogConverter;

    /**
     * 根据id查询customer，类型不固定???? 确定实现方式
     *
     * @param id
     * @return
     */
    @Override
    public AnimalDO findById(String id) {
        AnimalPO animalPO = animalPOMapper.selectById(id);
        if(null == animalPO) {
            return null;
        }

        AnimalTypeEnum type = AnimalTypeEnum.valueOf(animalPO.getType());
        switch (type) {
            case CAT:
                return catConverter.toDO(animalPO, catPOMapper.selectById(id));
            case DOG:
                return dogConverter.toDO(animalPO, dogPOMapper.selectById(id));
        }
        return null;
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public AnimalDO save(AnimalDO animalDO) {
        AnimalPO poParent = animalConverter.toPO(animalDO);
        if(null == poParent) {
            return null;
        }
        animalPOMapper.insert(poParent);

        if (animalDO instanceof CatDO) {
            CatPO catPO = catConverter.toPO((CatDO) animalDO);
            catPO.setId(poParent.getId());
            catPOMapper.insert(catPO);
            return catConverter.toDO(poParent, catPO);
        } else if (animalDO instanceof DogDO) {
            DogPO dogPO = dogConverter.toPO((DogDO) animalDO);
            dogPO.setId(poParent.getId());
            dogPOMapper.insert(dogPO);
            return dogConverter.toDO(poParent, dogPO);
        }

        return null;
    }

}
