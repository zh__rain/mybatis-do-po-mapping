package cn.com.agree.dopomapping.Inheritmapping.classtablemapping.interfaces.http;

import cn.com.agree.dopomapping.Inheritmapping.classtablemapping.application.dto.AnimalDTO;
import cn.com.agree.dopomapping.Inheritmapping.classtablemapping.application.service.AnimalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 类表映射 - 动物Controller
 */
@RestController
@RequestMapping("animals")
public class AnimalController {

    @Autowired
    private AnimalService animalService;

    /**
     * 获取动物信息
     * @param id 主键
     * @return 结果
     */
    @GetMapping("/{id}")
    public AnimalDTO findAnimal(@PathVariable("id") String id) {
        return animalService.findAnimal(id);
    }

    /**
     * 添加动物
     * @param animalDTO 动物信息
     * @return 结果
     */
    @PutMapping
    public AnimalDTO saveAnimal(@RequestBody AnimalDTO animalDTO) {
        return animalService.saveAnimal(animalDTO);
    }

}
