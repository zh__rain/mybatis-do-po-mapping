package cn.com.agree.dopomapping.Inheritmapping.classtablemapping;

// 类表继承。每个子类对应一张表，子类的公共属性存储在父类表中，而子类特有的属性存储在各自的表中。
// 【优点】数据表结构清晰，易于维护。
// 【缺点】查询完整数据需要join，影响性能。