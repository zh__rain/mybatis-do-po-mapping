package cn.com.agree.dopomapping.Inheritmapping.multitablemapping.application;

import cn.com.agree.dopomapping.Inheritmapping.multitablemapping.application.assembler.EmployeeAssembler;
import cn.com.agree.dopomapping.Inheritmapping.multitablemapping.application.dto.EmployeeDTO;
import cn.com.agree.dopomapping.Inheritmapping.multitablemapping.domain.EmployeeDO;
import cn.com.agree.dopomapping.Inheritmapping.multitablemapping.domain.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EmployeeService {

    @Autowired
    private EmployeeAssembler employeeAssembler;

    @Autowired
    private EmployeeRepository employeeRepository;

    public EmployeeDTO findEmployee(String id) {
        return employeeAssembler.toDTO(employeeRepository.findById(id));
    }

    public EmployeeDTO saveEmployee(EmployeeDTO employeeDTO) {
        EmployeeDO employeeDO = employeeAssembler.toEntity(employeeDTO);
        employeeDO = employeeRepository.save(employeeDO);
        return employeeAssembler.toDTO(employeeDO);
    }
}
