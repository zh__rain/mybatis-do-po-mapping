package cn.com.agree.dopomapping.Inheritmapping.multitablemapping.application.assembler;

import cn.com.agree.dopomapping.Inheritmapping.multitablemapping.application.dto.EmployeeDTO;
import cn.com.agree.dopomapping.Inheritmapping.multitablemapping.domain.EmployeeDO;
import cn.com.agree.dopomapping.Inheritmapping.multitablemapping.domain.ManagerDO;
import cn.com.agree.dopomapping.Inheritmapping.multitablemapping.domain.StaffDO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class EmployeeAssembler {

    @Autowired
    private ManagerAssembler managerAssembler;
    @Autowired
    private StaffAssembler staffAssembler;

    public EmployeeDTO toDTO(EmployeeDO employeeDO) {
        if (employeeDO instanceof ManagerDO) {
            return managerAssembler.toDTO((ManagerDO) employeeDO);
        } else if (employeeDO instanceof StaffDO) {
            return staffAssembler.toDTO((StaffDO) employeeDO);
        }

        return null;
    }

    public EmployeeDO toEntity(EmployeeDTO employeeDTO) {
        switch (employeeDTO.getEmployeeType()) {
            case MANAGER:
                return managerAssembler.toEntity(employeeDTO);
            case STAFF:
                return staffAssembler.toEntity(employeeDTO);
        }

        return null;
    }

}
