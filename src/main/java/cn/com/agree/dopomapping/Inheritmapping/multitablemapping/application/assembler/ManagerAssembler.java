package cn.com.agree.dopomapping.Inheritmapping.multitablemapping.application.assembler;

import cn.com.agree.dopomapping.Inheritmapping.multitablemapping.application.dto.EmployeeDTO;
import cn.com.agree.dopomapping.Inheritmapping.multitablemapping.domain.ManagerDO;
import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;

@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public interface ManagerAssembler {

    ManagerDO toEntity(EmployeeDTO managerDTO);

    EmployeeDTO toDTO(ManagerDO managerDO);

}
