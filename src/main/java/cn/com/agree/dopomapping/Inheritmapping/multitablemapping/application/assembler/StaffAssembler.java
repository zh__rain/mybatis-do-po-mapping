package cn.com.agree.dopomapping.Inheritmapping.multitablemapping.application.assembler;

import cn.com.agree.dopomapping.Inheritmapping.multitablemapping.application.dto.EmployeeDTO;
import cn.com.agree.dopomapping.Inheritmapping.multitablemapping.domain.StaffDO;
import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;

@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public interface StaffAssembler {

    StaffDO toEntity(EmployeeDTO employeeDTO);

    EmployeeDTO toDTO(StaffDO staffDO);

}
