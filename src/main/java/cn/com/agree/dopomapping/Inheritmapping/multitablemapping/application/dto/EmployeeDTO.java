package cn.com.agree.dopomapping.Inheritmapping.multitablemapping.application.dto;

import cn.com.agree.dopomapping.Inheritmapping.multitablemapping.domain.valueobj.EmployeeTypeEnum;
import lombok.Data;

@Data
public class EmployeeDTO {

    private String id;
    private String employeeId;
    private String fullName;
    private int age;
    private String department;
    private double salary;

    private EmployeeTypeEnum employeeType;

    private int teamSize;
    private String officeLocation;
    private boolean hasStockOptions;

    private String supervisorName;
    private boolean isFullTime;
    private String workstation;

}
