package cn.com.agree.dopomapping.Inheritmapping.multitablemapping.domain;

import cn.com.agree.dopomapping.Inheritmapping.multitablemapping.domain.valueobj.EmployeeTypeEnum;
import lombok.Data;

@Data
public class EmployeeDO {

    private String id;
    private String employeeId;
    private String fullName;
    private int age;
    private String department;
    private double salary;

    private EmployeeTypeEnum employeeType;

}
