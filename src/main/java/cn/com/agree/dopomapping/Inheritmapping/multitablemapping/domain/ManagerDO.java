package cn.com.agree.dopomapping.Inheritmapping.multitablemapping.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class ManagerDO extends EmployeeDO {

    private int teamSize;
    private String officeLocation;
    private boolean hasStockOptions;

}
