package cn.com.agree.dopomapping.Inheritmapping.multitablemapping.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class StaffDO extends EmployeeDO {

    private String supervisorName;
    private boolean isFullTime;
    private String workstation;

}
