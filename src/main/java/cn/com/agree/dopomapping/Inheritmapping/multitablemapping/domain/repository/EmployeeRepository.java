package cn.com.agree.dopomapping.Inheritmapping.multitablemapping.domain.repository;

import cn.com.agree.dopomapping.Inheritmapping.multitablemapping.domain.EmployeeDO;

public interface EmployeeRepository {

    EmployeeDO findById(String id);
    EmployeeDO save(EmployeeDO employeeDO);

}
