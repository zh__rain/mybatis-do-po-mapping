package cn.com.agree.dopomapping.Inheritmapping.multitablemapping.infrastructure.converter;

import cn.com.agree.dopomapping.Inheritmapping.multitablemapping.domain.EmployeeDO;
import cn.com.agree.dopomapping.Inheritmapping.multitablemapping.domain.ManagerDO;
import cn.com.agree.dopomapping.Inheritmapping.multitablemapping.domain.valueobj.EmployeeTypeEnum;
import cn.com.agree.dopomapping.Inheritmapping.multitablemapping.infrastructure.po.ManagerPO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingConstants;

@Mapper(componentModel = MappingConstants.ComponentModel.SPRING, imports = {EmployeeTypeEnum.class})
public interface ManagerConverter {

    ManagerPO toPO(EmployeeDO managerDO);

    @Mapping(target = "employeeType", expression = "java(EmployeeTypeEnum.MANAGER)")
    ManagerDO toDO(ManagerPO managerPO);

}
