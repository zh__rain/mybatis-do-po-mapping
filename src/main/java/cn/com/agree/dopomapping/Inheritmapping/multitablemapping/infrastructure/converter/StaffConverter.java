package cn.com.agree.dopomapping.Inheritmapping.multitablemapping.infrastructure.converter;

import cn.com.agree.dopomapping.Inheritmapping.multitablemapping.domain.EmployeeDO;
import cn.com.agree.dopomapping.Inheritmapping.multitablemapping.domain.StaffDO;
import cn.com.agree.dopomapping.Inheritmapping.multitablemapping.domain.valueobj.EmployeeTypeEnum;
import cn.com.agree.dopomapping.Inheritmapping.multitablemapping.infrastructure.po.StaffPO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingConstants;

/**
 * @author zc
 * @date 2023/8/22 17:46
 * @description
 */
@Mapper(componentModel = MappingConstants.ComponentModel.SPRING, imports = {EmployeeTypeEnum.class})
public interface StaffConverter {

    StaffPO toPO(EmployeeDO staffDO);

    @Mapping(target = "employeeType", expression = "java(EmployeeTypeEnum.STAFF)")
    StaffDO toDO(StaffPO staffPO);

}
