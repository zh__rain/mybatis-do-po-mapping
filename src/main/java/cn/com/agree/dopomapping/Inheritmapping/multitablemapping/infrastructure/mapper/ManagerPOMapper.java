package cn.com.agree.dopomapping.Inheritmapping.multitablemapping.infrastructure.mapper;

import cn.com.agree.dopomapping.Inheritmapping.multitablemapping.infrastructure.po.ManagerPO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface ManagerPOMapper extends BaseMapper<ManagerPO> {
}
