package cn.com.agree.dopomapping.Inheritmapping.multitablemapping.infrastructure.mapper;

import cn.com.agree.dopomapping.Inheritmapping.multitablemapping.infrastructure.po.StaffPO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface StaffPOMapper extends BaseMapper<StaffPO> {
}
