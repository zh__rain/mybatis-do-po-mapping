package cn.com.agree.dopomapping.Inheritmapping.multitablemapping.infrastructure.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName("tb_manager")
public class ManagerPO {

    @TableId(type = IdType.ASSIGN_ID)
    private String id;
    private String employeeId;
    private String fullName;
    private int age;
    private String department;
    private double salary;

    private int teamSize;
    private String officeLocation;
    private boolean hasStockOptions;

}
