package cn.com.agree.dopomapping.Inheritmapping.multitablemapping.infrastructure.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName("tb_staff")
public class StaffPO {

    @TableId(type = IdType.ASSIGN_ID)
    private String id;
    private String employeeId;
    private String fullName;
    private int age;
    private String department;
    private double salary;

    private String supervisorName;
    private boolean isFullTime;
    private String workstation;
}
