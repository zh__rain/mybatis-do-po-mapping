package cn.com.agree.dopomapping.Inheritmapping.multitablemapping.interfaces.http;

import cn.com.agree.dopomapping.Inheritmapping.multitablemapping.application.EmployeeService;
import cn.com.agree.dopomapping.Inheritmapping.multitablemapping.application.dto.EmployeeDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 多表映射 - 职工Controller
 */
@RestController
@RequestMapping("employees")
public class EmployeeController {

    @Autowired
    private EmployeeService employeeService;

    /**
     * 获取职工信息
     * @param id 主键
     * @return 结果
     */
    @GetMapping("/{id}")
    public EmployeeDTO findEmployee(@PathVariable("id") String id) {
        return employeeService.findEmployee(id);
    }

    /**
     * 添加职工
     * @param employeeDTO 职工信息
     * @return 结果
     */
    @PutMapping
    public EmployeeDTO saveEmployee(@RequestBody EmployeeDTO employeeDTO) {
        return employeeService.saveEmployee(employeeDTO);
    }

}
