package cn.com.agree.dopomapping.Inheritmapping.multitablemapping;

// 多表继承。每个子类对应一张表，子类的属性直接存储在各自的表中，同时也将父类的公共属性存储在子表中。
// 【优点】扩展性较好，新增类别只需要新增对应的数据表；
// 【缺点】增加公用字段需要每个表都需要进行DDL操作，维护成本高。