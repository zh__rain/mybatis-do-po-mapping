package cn.com.agree.dopomapping.Inheritmapping.singletontablemapping.application.assembler;

import cn.com.agree.dopomapping.Inheritmapping.singletontablemapping.application.dto.OrderDTO;
import cn.com.agree.dopomapping.Inheritmapping.singletontablemapping.domain.InStoreOrderDO;
import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;

@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public interface InStoreOrderAssembler {

    InStoreOrderDO toEntity(OrderDTO orderDTO);

    OrderDTO toDTO(InStoreOrderDO inStoreOrderDO);

}
