package cn.com.agree.dopomapping.Inheritmapping.singletontablemapping.application.assembler;

import cn.com.agree.dopomapping.Inheritmapping.singletontablemapping.application.dto.OrderDTO;
import cn.com.agree.dopomapping.Inheritmapping.singletontablemapping.domain.OnlineOrderDO;
import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;

@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public interface OnlineOrderAssembler {

    OnlineOrderDO toEntity(OrderDTO orderDTO);

    OrderDTO toDTO(OnlineOrderDO onlineOrderDO);

}
