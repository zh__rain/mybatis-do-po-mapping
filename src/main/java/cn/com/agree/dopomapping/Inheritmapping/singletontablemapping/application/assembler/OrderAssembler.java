package cn.com.agree.dopomapping.Inheritmapping.singletontablemapping.application.assembler;

import cn.com.agree.dopomapping.Inheritmapping.singletontablemapping.application.dto.OrderDTO;
import cn.com.agree.dopomapping.Inheritmapping.singletontablemapping.domain.InStoreOrderDO;
import cn.com.agree.dopomapping.Inheritmapping.singletontablemapping.domain.OnlineOrderDO;
import cn.com.agree.dopomapping.Inheritmapping.singletontablemapping.domain.OrderDO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class OrderAssembler {

    @Autowired
    private InStoreOrderAssembler inStoreOrderAssembler;
    @Autowired
    private OnlineOrderAssembler onlineOrderAssembler;

    public OrderDTO toDTO(OrderDO orderDO) {
        if (orderDO instanceof InStoreOrderDO) {
            return inStoreOrderAssembler.toDTO((InStoreOrderDO) orderDO);
        } else if (orderDO instanceof OnlineOrderDO) {
            return onlineOrderAssembler.toDTO((OnlineOrderDO) orderDO);
        }

        return null;
    }

    public OrderDO toEntity(OrderDTO orderDTO) {
        switch (orderDTO.getOrderType()) {
            case ONLINE_ORDER:
                return onlineOrderAssembler.toEntity(orderDTO);
            case IN_STORE_ORDER:
                return inStoreOrderAssembler.toEntity(orderDTO);
        }

        return null;
    }

}
