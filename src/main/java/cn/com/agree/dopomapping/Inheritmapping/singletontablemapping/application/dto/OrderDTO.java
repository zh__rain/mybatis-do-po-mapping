package cn.com.agree.dopomapping.Inheritmapping.singletontablemapping.application.dto;

import cn.com.agree.dopomapping.Inheritmapping.singletontablemapping.domain.valueobj.OrderTypeEnum;
import lombok.Data;

@Data
public class OrderDTO {

    private String id;
    private String orderId;
    private double amount;
    private String customerName;
    private String date;

    private OrderTypeEnum orderType;

    private String storeLocation;
    private String salespersonName;
    private boolean isMembershipDiscount;

    private String paymentMethod;
    private String shippingAddress;
    private boolean isGift;

}
