package cn.com.agree.dopomapping.Inheritmapping.singletontablemapping.application.service;

import cn.com.agree.dopomapping.Inheritmapping.singletontablemapping.application.assembler.OrderAssembler;
import cn.com.agree.dopomapping.Inheritmapping.singletontablemapping.application.dto.OrderDTO;
import cn.com.agree.dopomapping.Inheritmapping.singletontablemapping.domain.OrderDO;
import cn.com.agree.dopomapping.Inheritmapping.singletontablemapping.domain.repository.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OrderService {

    @Autowired
    private OrderAssembler orderAssembler;

    @Autowired
    private OrderRepository orderRepository;

    public OrderDTO findOrder(String id) {
        return orderAssembler.toDTO(orderRepository.findById(id));
    }

    public OrderDTO saveOrder(OrderDTO orderDTO) {
        OrderDO orderDO = orderAssembler.toEntity(orderDTO);
        orderDO = orderRepository.save(orderDO);
        return orderAssembler.toDTO(orderDO);
    }
}
