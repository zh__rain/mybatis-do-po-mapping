package cn.com.agree.dopomapping.Inheritmapping.singletontablemapping.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class InStoreOrderDO extends OrderDO {

    private String storeLocation;
    private String salespersonName;
    private boolean isMembershipDiscount;

}
