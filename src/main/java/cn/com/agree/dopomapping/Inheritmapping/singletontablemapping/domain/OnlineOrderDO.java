package cn.com.agree.dopomapping.Inheritmapping.singletontablemapping.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class OnlineOrderDO extends OrderDO {



    private String paymentMethod;
    private String shippingAddress;
    private boolean isGift;

}
