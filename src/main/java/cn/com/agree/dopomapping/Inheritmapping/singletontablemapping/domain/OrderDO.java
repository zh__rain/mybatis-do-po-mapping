package cn.com.agree.dopomapping.Inheritmapping.singletontablemapping.domain;

import cn.com.agree.dopomapping.Inheritmapping.singletontablemapping.domain.valueobj.OrderTypeEnum;
import lombok.Data;

@Data
public class OrderDO {

    private Integer id;
    private String orderId;
    private double amount;
    private String customerName;
    private String date;

    private OrderTypeEnum orderType; // 区分哪种订单
}
