package cn.com.agree.dopomapping.Inheritmapping.singletontablemapping.domain.repository;

import cn.com.agree.dopomapping.Inheritmapping.singletontablemapping.domain.OrderDO;

public interface OrderRepository {

    OrderDO findById(String id);

    OrderDO save(OrderDO orderDO);

}
