package cn.com.agree.dopomapping.Inheritmapping.singletontablemapping.infrastructure.converter;

import cn.com.agree.dopomapping.Inheritmapping.singletontablemapping.domain.InStoreOrderDO;
import cn.com.agree.dopomapping.Inheritmapping.singletontablemapping.infrastructure.po.OrderPO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface InStoreOrderConverter {

    OrderPO toPO(InStoreOrderDO inStoreOrderDO);

    InStoreOrderDO toEntity(OrderPO orderPO);

}
