package cn.com.agree.dopomapping.Inheritmapping.singletontablemapping.infrastructure.converter;

import cn.com.agree.dopomapping.Inheritmapping.singletontablemapping.domain.OnlineOrderDO;
import cn.com.agree.dopomapping.Inheritmapping.singletontablemapping.infrastructure.po.OrderPO;
import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;

@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public interface OnlineOrderConverter {

    OrderPO toPO(OnlineOrderDO onlineOrderDO);

    OnlineOrderDO toEntity(OrderPO orderPO);

}
