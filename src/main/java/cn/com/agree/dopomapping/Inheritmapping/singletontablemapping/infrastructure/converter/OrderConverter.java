package cn.com.agree.dopomapping.Inheritmapping.singletontablemapping.infrastructure.converter;

import cn.com.agree.dopomapping.Inheritmapping.singletontablemapping.domain.InStoreOrderDO;
import cn.com.agree.dopomapping.Inheritmapping.singletontablemapping.domain.OnlineOrderDO;
import cn.com.agree.dopomapping.Inheritmapping.singletontablemapping.domain.OrderDO;
import cn.com.agree.dopomapping.Inheritmapping.singletontablemapping.domain.valueobj.OrderTypeEnum;
import cn.com.agree.dopomapping.Inheritmapping.singletontablemapping.infrastructure.po.OrderPO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Objects;

@Component
public class OrderConverter {

    @Autowired
    private InStoreOrderConverter inStoreOrderConverter;
    @Autowired
    private OnlineOrderConverter onlineOrderConverter;

    public OrderPO toPO(OrderDO orderDO) {
        if (orderDO instanceof InStoreOrderDO) {
            return inStoreOrderConverter.toPO((InStoreOrderDO) orderDO);
        } else if (orderDO instanceof OnlineOrderDO) {
            return onlineOrderConverter.toPO((OnlineOrderDO) orderDO);
        }

        return null;
    }
    public OrderDO toEntity(OrderPO orderPO) {
        if (Objects.isNull(orderPO)) {
            return null;
        }
        OrderTypeEnum orderType = OrderTypeEnum.valueOf(orderPO.getOrderType());
        switch (orderType) {
            case IN_STORE_ORDER:
                return inStoreOrderConverter.toEntity(orderPO);
            case ONLINE_ORDER:
                return onlineOrderConverter.toEntity(orderPO);
        }
        return null;
    }

}
