package cn.com.agree.dopomapping.Inheritmapping.singletontablemapping.infrastructure.mapper;

import cn.com.agree.dopomapping.Inheritmapping.singletontablemapping.infrastructure.po.OrderPO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface OrderPOMapper extends BaseMapper<OrderPO> {
}
