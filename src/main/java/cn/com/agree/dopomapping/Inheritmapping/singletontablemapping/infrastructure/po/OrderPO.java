package cn.com.agree.dopomapping.Inheritmapping.singletontablemapping.infrastructure.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@TableName("tb_order")
@Data
public class OrderPO {

    // 公共属性
    @TableId(type = IdType.AUTO)
    private Integer id;

    @TableField("order_id")
    private String orderId;

    private double amount;

    @TableField("customer_name")
    private String customerName;

    private String date;

    @TableField("order_type")
    private String orderType;

    // 在线订单属性
    @TableField("payment_method")
    private String paymentMethod;

    @TableField("shipping_address")
    private String shippingAddress;

    @TableField("is_gift")
    private boolean isGift;

    // 实体店订单属性
    @TableField("store_location")
    private String storeLocation;

    @TableField("salesperson_name")
    private String salespersonName;

    @TableField("is_membership_discount")
    private boolean isMembershipDiscount;

}
