package cn.com.agree.dopomapping.Inheritmapping.singletontablemapping.infrastructure.repositoryimpl;

import cn.com.agree.dopomapping.Inheritmapping.singletontablemapping.domain.OrderDO;
import cn.com.agree.dopomapping.Inheritmapping.singletontablemapping.domain.repository.OrderRepository;
import cn.com.agree.dopomapping.Inheritmapping.singletontablemapping.infrastructure.converter.OrderConverter;
import cn.com.agree.dopomapping.Inheritmapping.singletontablemapping.infrastructure.mapper.OrderPOMapper;
import cn.com.agree.dopomapping.Inheritmapping.singletontablemapping.infrastructure.po.OrderPO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class OrderRepositoryImpl implements OrderRepository {

    @Autowired
    private OrderPOMapper orderPOMapper;
    @Autowired
    private OrderConverter customerConverter;

    @Override
    public OrderDO findById(String id) {
        return customerConverter.toEntity(orderPOMapper.selectById(id));
    }

    @Override
    public OrderDO save(OrderDO orderDO) {
        OrderPO customerPO = customerConverter.toPO(orderDO);
        orderPOMapper.insert(customerPO);
        return customerConverter.toEntity(customerPO);
    }
}
