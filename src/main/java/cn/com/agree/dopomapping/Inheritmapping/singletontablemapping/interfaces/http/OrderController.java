package cn.com.agree.dopomapping.Inheritmapping.singletontablemapping.interfaces.http;

import cn.com.agree.dopomapping.Inheritmapping.singletontablemapping.application.dto.OrderDTO;
import cn.com.agree.dopomapping.Inheritmapping.singletontablemapping.application.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 单表映射 - 订单Controller
 */
@RestController
@RequestMapping("orders")
public class OrderController {

    @Autowired
    private OrderService orderService;

    /**
     * 获取订单信息
     * @param id 主键
     * @return 结果
     */
    @GetMapping("/{id}")
    public OrderDTO findOrder(@PathVariable("id") String id) {
        return orderService.findOrder(id);
    }

    /**
     * 添加订单
     * @param orderDTO 订单信息
     * @return 结果
     */
    @PutMapping
    public OrderDTO saveOrder(@RequestBody OrderDTO orderDTO) {
        return orderService.saveOrder(orderDTO);
    }

}
