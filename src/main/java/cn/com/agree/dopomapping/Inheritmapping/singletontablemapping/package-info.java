package cn.com.agree.dopomapping.Inheritmapping.singletontablemapping;

// 单表继承。在同一张表中存储所有的数据，通过一个类型字段来区分不同类型的数据。
// 【优点】可以简化数据库设计和查询操作。
// 【缺点】会造成数据冗余和查询性能的下降。