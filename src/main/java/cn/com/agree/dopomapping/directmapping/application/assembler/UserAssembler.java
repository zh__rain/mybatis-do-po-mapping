package cn.com.agree.dopomapping.directmapping.application.assembler;

import cn.com.agree.dopomapping.directmapping.application.dto.UserDTO;
import cn.com.agree.dopomapping.directmapping.domain.entity.UserDO;
import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;

@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public interface UserAssembler {

    UserDO toEntity(UserDTO userDTO);

    UserDTO toDTO(UserDO userDO);

}
