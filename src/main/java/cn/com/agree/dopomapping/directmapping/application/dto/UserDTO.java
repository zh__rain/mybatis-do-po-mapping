package cn.com.agree.dopomapping.directmapping.application.dto;

import lombok.Data;
import org.jetbrains.annotations.NotNull;

/**
 * 用户信息
 */
@Data
public class UserDTO {

    private Long id;

    private String name;

    private Integer age;

}
