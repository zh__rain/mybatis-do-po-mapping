package cn.com.agree.dopomapping.directmapping.application.service;

import cn.com.agree.dopomapping.directmapping.application.assembler.UserAssembler;
import cn.com.agree.dopomapping.directmapping.application.dto.UserDTO;
import cn.com.agree.dopomapping.directmapping.domain.entity.UserDO;
import cn.com.agree.dopomapping.directmapping.domain.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private UserAssembler userAssembler;

    public UserDTO findUser(String id) {
        return userAssembler.toDTO(userRepository.findById(id));
    }

    public UserDTO addUser(UserDTO userDTO) {
        UserDO userDO = userAssembler.toEntity(userDTO);
        userDO = userRepository.save(userDO);
        return userAssembler.toDTO(userDO);
    }

}
