package cn.com.agree.dopomapping.directmapping.domain.entity;

import lombok.Data;

@Data
public class UserDO {

    private Long id;

    private String name;

    private Integer age;

}
