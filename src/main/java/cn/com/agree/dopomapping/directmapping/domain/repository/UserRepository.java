package cn.com.agree.dopomapping.directmapping.domain.repository;

import cn.com.agree.dopomapping.directmapping.domain.entity.UserDO;

public interface UserRepository {

    UserDO findById(String id);

    UserDO save(UserDO userDO);

}
