package cn.com.agree.dopomapping.directmapping.infrastructure.converter;

import cn.com.agree.dopomapping.directmapping.domain.entity.UserDO;
import cn.com.agree.dopomapping.directmapping.infrastructure.po.UserPO;
import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;

@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public interface DirectMappingUserConverter {

    UserPO toPO(UserDO user);

    UserDO toEntity(UserPO userPO);

}
