package cn.com.agree.dopomapping.directmapping.infrastructure.mapper;

import cn.com.agree.dopomapping.directmapping.infrastructure.po.UserPO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface DirectMappingUserPOMapper extends BaseMapper<UserPO> {
}
