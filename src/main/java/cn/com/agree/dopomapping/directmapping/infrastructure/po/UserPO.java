package cn.com.agree.dopomapping.directmapping.infrastructure.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName("tb_user")
public class UserPO {

    @TableId(type = IdType.AUTO)
    private Long id;

    private String name;

    private Integer age;



}
