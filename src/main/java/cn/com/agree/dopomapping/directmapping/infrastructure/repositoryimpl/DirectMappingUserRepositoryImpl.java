package cn.com.agree.dopomapping.directmapping.infrastructure.repositoryimpl;

import cn.com.agree.dopomapping.directmapping.domain.entity.UserDO;
import cn.com.agree.dopomapping.directmapping.domain.repository.UserRepository;
import cn.com.agree.dopomapping.directmapping.infrastructure.converter.DirectMappingUserConverter;
import cn.com.agree.dopomapping.directmapping.infrastructure.mapper.DirectMappingUserPOMapper;
import cn.com.agree.dopomapping.directmapping.infrastructure.po.UserPO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.Objects;

@Repository
public class DirectMappingUserRepositoryImpl implements UserRepository {

    @Autowired
    private DirectMappingUserPOMapper directMappingUserPOMapper;

    @Autowired
    private DirectMappingUserConverter directMappingUserConverter;

    @Override
    public UserDO findById(String id) {
        UserPO userPO = directMappingUserPOMapper.selectById(id);
        if (Objects.isNull(userPO)) {
            return null;
        }
        return directMappingUserConverter.toEntity(userPO);
    }

    @Override
    public UserDO save(UserDO user) {
        UserPO userPO = directMappingUserConverter.toPO(user);
        directMappingUserPOMapper.insert(userPO);
        return directMappingUserConverter.toEntity(userPO);
    }

}
