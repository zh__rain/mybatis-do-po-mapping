package cn.com.agree.dopomapping.directmapping.interfaces.http;

import cn.com.agree.dopomapping.directmapping.application.dto.UserDTO;
import cn.com.agree.dopomapping.directmapping.application.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 用户Controller
 * @author zhangyu
 */
@RestController
@RequestMapping("users")
public class UserController {

    @Autowired
    private UserService userService;

    /**
     * 查询用户
     * @param id 用户id
     * @return 结果
     */
    @GetMapping("/{id}")
    public UserDTO findUser(@PathVariable("id") String id) {
        return userService.findUser(id);
    }

    /**
     * 添加用户
     * @param userDTO 新用户信息
     */
    @PutMapping
    public UserDTO addUser(@RequestBody UserDTO userDTO) {
        return userService.addUser(userDTO);
    }

}
