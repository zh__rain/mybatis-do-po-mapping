package cn.com.agree.dopomapping.valueobjectmapping.expandmapping.application.assembler;

import cn.com.agree.dopomapping.valueobjectmapping.expandmapping.application.dto.CarDTO;
import cn.com.agree.dopomapping.valueobjectmapping.expandmapping.domain.CarDO;
import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;

@Mapper(componentModel = MappingConstants.ComponentModel.SPRING, uses = {EngineAssembler.class})
public interface CarAssembler {

    CarDTO toDTO(CarDO user);

    CarDO toEntity(CarDTO userDTO);

}
