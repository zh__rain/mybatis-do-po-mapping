package cn.com.agree.dopomapping.valueobjectmapping.expandmapping.application.assembler;

import cn.com.agree.dopomapping.valueobjectmapping.expandmapping.application.dto.EngineDTO;
import cn.com.agree.dopomapping.valueobjectmapping.expandmapping.domain.valueobject.EngineValueObj;
import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;

@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public interface EngineAssembler {

    EngineDTO toDTO(EngineValueObj engineValueObj);

    EngineValueObj toEntity(EngineDTO engineDTO);

}
