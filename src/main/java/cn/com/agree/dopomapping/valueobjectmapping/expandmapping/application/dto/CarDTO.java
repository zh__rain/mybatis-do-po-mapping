package cn.com.agree.dopomapping.valueobjectmapping.expandmapping.application.dto;

import lombok.Data;

@Data
public class CarDTO {

    private String id;          // 主键

    private String make;       // 制造商

    private String model;      // 型号

    private int year;           // 出厂年份

    private String color;      // 颜色

    private double price;      // 价格

    private EngineDTO engine;   // 引擎

}
