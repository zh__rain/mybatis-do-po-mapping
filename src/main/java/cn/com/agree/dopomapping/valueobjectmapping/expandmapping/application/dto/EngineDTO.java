package cn.com.agree.dopomapping.valueobjectmapping.expandmapping.application.dto;

import lombok.Data;

@Data
public class EngineDTO {

    private String type;        // 引擎类型
    private int horsepower;     // 马力

}
