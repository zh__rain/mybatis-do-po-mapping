package cn.com.agree.dopomapping.valueobjectmapping.expandmapping.application.service;

import cn.com.agree.dopomapping.valueobjectmapping.expandmapping.application.assembler.CarAssembler;
import cn.com.agree.dopomapping.valueobjectmapping.expandmapping.application.dto.CarDTO;
import cn.com.agree.dopomapping.valueobjectmapping.expandmapping.domain.CarDO;
import cn.com.agree.dopomapping.valueobjectmapping.expandmapping.domain.repository.CarRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CarService {

    @Autowired
    private CarAssembler carAssembler;

    @Autowired
    private CarRepository carRepository;


    public CarDTO findCar(String id) {
        return carAssembler.toDTO(carRepository.findById(id));
    }

    public CarDTO addCar(CarDTO carDTO) {
        CarDO carDO = carAssembler.toEntity(carDTO);
        carDO = carRepository.save(carDO);
        return carAssembler.toDTO(carDO);
    }
}
