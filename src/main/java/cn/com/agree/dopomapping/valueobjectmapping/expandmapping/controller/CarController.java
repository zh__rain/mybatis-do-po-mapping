package cn.com.agree.dopomapping.valueobjectmapping.expandmapping.controller;

import cn.com.agree.dopomapping.valueobjectmapping.expandmapping.application.dto.CarDTO;
import cn.com.agree.dopomapping.valueobjectmapping.expandmapping.application.service.CarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 值对象展开平铺 - 汽车Controller
 */
@RestController
@RequestMapping("cars")
public class CarController {

    @Autowired
    private CarService carService;

    /**
     * 查询汽车信息
     * @param id 主键
     * @return 结果
     */
    @GetMapping("/{id}")
    public CarDTO findCar(@PathVariable("id") String id) {
        return carService.findCar(id);
    }

    /**
     * 添加汽车
     * @param carDTO 车辆信息
     * @return 结果
     */
    @PutMapping
    public CarDTO addCar(@RequestBody CarDTO carDTO) {
        return carService.addCar(carDTO);
    }

}
