package cn.com.agree.dopomapping.valueobjectmapping.expandmapping.domain;

import cn.com.agree.dopomapping.valueobjectmapping.expandmapping.domain.valueobject.EngineValueObj;
import lombok.Data;

/**
 * 汽车信息
 */
@Data
public class CarDO {

    private String id;          // 主键

    private String make;       // 制造商

    private String model;      // 型号

    private int year;           // 出厂年份

    private String color;      // 颜色

    private double price;      // 价格

    private EngineValueObj engine;     // 嵌套的引擎信息

}
