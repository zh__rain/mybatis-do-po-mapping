package cn.com.agree.dopomapping.valueobjectmapping.expandmapping.domain.repository;


import cn.com.agree.dopomapping.valueobjectmapping.expandmapping.domain.CarDO;

public interface CarRepository {

    CarDO findById(String id);

    CarDO save(CarDO user);

}
