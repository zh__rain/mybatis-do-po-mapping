package cn.com.agree.dopomapping.valueobjectmapping.expandmapping.domain.valueobject;

import lombok.Data;

/**
 * 发动机信息
 */
@Data
public class EngineValueObj {

    private String type;        // 引擎类型
    private int horsepower;     // 马力

}
