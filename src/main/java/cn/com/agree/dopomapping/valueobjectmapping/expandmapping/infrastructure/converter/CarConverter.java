package cn.com.agree.dopomapping.valueobjectmapping.expandmapping.infrastructure.converter;

import cn.com.agree.dopomapping.valueobjectmapping.expandmapping.domain.CarDO;
import cn.com.agree.dopomapping.valueobjectmapping.expandmapping.infrastructure.po.CarPO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingConstants;

@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public interface CarConverter {

    @Mapping(target = "type", source = "engine.type")
    @Mapping(target = "horsepower", source = "engine.horsepower")
    CarPO toPO(CarDO carDO);

    @Mapping(target = "engine.type", source = "type")
    @Mapping(target = "engine.horsepower", source = "horsepower")
    CarDO toEntity(CarPO carPO);

}
