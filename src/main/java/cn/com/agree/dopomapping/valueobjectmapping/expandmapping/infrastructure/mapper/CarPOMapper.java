package cn.com.agree.dopomapping.valueobjectmapping.expandmapping.infrastructure.mapper;

import cn.com.agree.dopomapping.valueobjectmapping.expandmapping.infrastructure.po.CarPO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface CarPOMapper extends BaseMapper<CarPO> {
}
