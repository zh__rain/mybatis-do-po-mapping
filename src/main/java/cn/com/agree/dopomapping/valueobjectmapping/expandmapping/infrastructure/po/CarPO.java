package cn.com.agree.dopomapping.valueobjectmapping.expandmapping.infrastructure.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName("tb_car")
public class CarPO {

    @TableId(type = IdType.AUTO)
    private Long id;

    private String make;       // 制造商
    private String model;      // 型号
    private int year;           // 出厂年份
    private String color;      // 颜色
    private double price;      // 价格

    private String type;        // 引擎类型
    private int horsepower;     // 马力

}
