package cn.com.agree.dopomapping.valueobjectmapping.expandmapping.infrastructure.repositoryimpl;

import cn.com.agree.dopomapping.valueobjectmapping.expandmapping.domain.CarDO;
import cn.com.agree.dopomapping.valueobjectmapping.expandmapping.domain.repository.CarRepository;
import cn.com.agree.dopomapping.valueobjectmapping.expandmapping.infrastructure.converter.CarConverter;
import cn.com.agree.dopomapping.valueobjectmapping.expandmapping.infrastructure.mapper.CarPOMapper;
import cn.com.agree.dopomapping.valueobjectmapping.expandmapping.infrastructure.po.CarPO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class CarRepositoryImpl implements CarRepository {

    @Autowired
    private CarPOMapper carPOMapper;
    @Autowired
    private CarConverter carConverter;

    @Override
    public CarDO findById(String id) {
        return carConverter.toEntity(carPOMapper.selectById(id));
    }

    @Override
    public CarDO save(CarDO carDO) {
        CarPO carPO = carConverter.toPO(carDO);
        carPOMapper.insert(carPO);
        return carConverter.toEntity(carPO);
    }

}
