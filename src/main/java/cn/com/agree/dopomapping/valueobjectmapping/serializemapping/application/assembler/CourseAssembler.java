package cn.com.agree.dopomapping.valueobjectmapping.serializemapping.application.assembler;

import cn.com.agree.dopomapping.valueobjectmapping.serializemapping.domain.valueobject.CourseValueObj;
import cn.com.agree.dopomapping.valueobjectmapping.serializemapping.application.dto.CourseDTO;
import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;

@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public interface CourseAssembler {

    CourseDTO toDTO(CourseValueObj courseValueObj);

    CourseValueObj toEntity(CourseDTO courseDTO);

}
