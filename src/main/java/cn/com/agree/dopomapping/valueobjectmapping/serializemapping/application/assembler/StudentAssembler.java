package cn.com.agree.dopomapping.valueobjectmapping.serializemapping.application.assembler;

import cn.com.agree.dopomapping.valueobjectmapping.serializemapping.domain.StudentDO;
import cn.com.agree.dopomapping.valueobjectmapping.serializemapping.application.dto.StudentDTO;
import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;

@Mapper(componentModel = MappingConstants.ComponentModel.SPRING , uses = {CourseAssembler.class})
public interface StudentAssembler {

    StudentDTO toDTO(StudentDO studentDO);

    StudentDO toEntity(StudentDTO studentDTO);

}
