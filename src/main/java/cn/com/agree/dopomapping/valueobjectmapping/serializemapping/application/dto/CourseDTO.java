package cn.com.agree.dopomapping.valueobjectmapping.serializemapping.application.dto;

import lombok.Data;

@Data
public class CourseDTO {


    private String courseCode;

    private String courseName;

    private int creditHours;

    private String instructor;


}
