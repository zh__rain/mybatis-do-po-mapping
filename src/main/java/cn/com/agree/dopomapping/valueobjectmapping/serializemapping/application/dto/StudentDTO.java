package cn.com.agree.dopomapping.valueobjectmapping.serializemapping.application.dto;

import lombok.Data;

import java.util.List;

/**
 * 学生信息
 */
@Data
public class StudentDTO {

    private String id;

    private String name;

    private Integer age;

    private List<CourseDTO> courses;

}
