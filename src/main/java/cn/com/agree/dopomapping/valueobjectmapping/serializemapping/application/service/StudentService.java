package cn.com.agree.dopomapping.valueobjectmapping.serializemapping.application.service;

import cn.com.agree.dopomapping.valueobjectmapping.serializemapping.application.assembler.StudentAssembler;
import cn.com.agree.dopomapping.valueobjectmapping.serializemapping.domain.StudentDO;
import cn.com.agree.dopomapping.valueobjectmapping.serializemapping.domain.repository.StudentRepository;
import cn.com.agree.dopomapping.valueobjectmapping.serializemapping.application.dto.StudentDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class StudentService {

    @Autowired
    private StudentRepository studentRepository;

    @Autowired
    private StudentAssembler studentAssembler;

    public StudentDTO findStudent(String id) {
        return studentAssembler.toDTO(studentRepository.findById(id));
    }

    public StudentDTO addStudent(StudentDTO studentDTO) {
        StudentDO studentDO = studentAssembler.toEntity(studentDTO);
        studentDO = studentRepository.save(studentDO);
        return studentAssembler.toDTO(studentDO);
    }
}
