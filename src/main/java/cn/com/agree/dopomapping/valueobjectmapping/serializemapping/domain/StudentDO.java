package cn.com.agree.dopomapping.valueobjectmapping.serializemapping.domain;

import cn.com.agree.dopomapping.valueobjectmapping.serializemapping.domain.valueobject.CourseValueObj;
import lombok.Data;

import java.util.List;

@Data
public class StudentDO {

    private String id;

    private String name;

    private Integer age;

    private List<CourseValueObj> courses;

}
