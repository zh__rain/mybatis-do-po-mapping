package cn.com.agree.dopomapping.valueobjectmapping.serializemapping.domain.repository;


import cn.com.agree.dopomapping.valueobjectmapping.serializemapping.domain.StudentDO;

public interface StudentRepository {

    StudentDO findById(String id);

    StudentDO save(StudentDO studentPO);

}
