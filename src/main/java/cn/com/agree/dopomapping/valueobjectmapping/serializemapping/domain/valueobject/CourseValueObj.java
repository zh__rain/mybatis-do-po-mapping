package cn.com.agree.dopomapping.valueobjectmapping.serializemapping.domain.valueobject;

import lombok.Data;

@Data
public class CourseValueObj {

    private String courseCode;

    private String courseName;

    private int creditHours;

    private String instructor;

}
