package cn.com.agree.dopomapping.valueobjectmapping.serializemapping.infrastructure.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName("tb_student")
public class StudentPO {

    @TableId(type = IdType.AUTO)
    private Long id;

    private String name;

    private Integer age;

    private String courses;

}
