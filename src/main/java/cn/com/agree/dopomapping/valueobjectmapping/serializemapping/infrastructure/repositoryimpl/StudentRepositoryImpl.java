package cn.com.agree.dopomapping.valueobjectmapping.serializemapping.infrastructure.repositoryimpl;

import cn.com.agree.dopomapping.valueobjectmapping.serializemapping.domain.StudentDO;
import cn.com.agree.dopomapping.valueobjectmapping.serializemapping.domain.repository.StudentRepository;
import cn.com.agree.dopomapping.valueobjectmapping.serializemapping.infrastructure.po.StudentPO;
import cn.com.agree.dopomapping.valueobjectmapping.serializemapping.infrastructure.converter.StudentConverter;
import cn.com.agree.dopomapping.valueobjectmapping.serializemapping.infrastructure.mapper.StudentPoMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class StudentRepositoryImpl implements StudentRepository {

    @Autowired
    private StudentPoMapper studentPoMapper;
    @Autowired
    private StudentConverter studentConverter;

    @Override
    public StudentDO findById(String id) {
        return studentConverter.toEntity(studentPoMapper.selectById(id));
    }

    @Override
    public StudentDO save(StudentDO studentDO) {
        StudentPO studentPO = studentConverter.toPO(studentDO);
        studentPoMapper.insert(studentPO);
        return studentConverter.toEntity(studentPO);
    }

}
