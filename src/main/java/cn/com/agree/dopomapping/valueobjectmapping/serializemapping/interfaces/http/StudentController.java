package cn.com.agree.dopomapping.valueobjectmapping.serializemapping.interfaces.http;

import cn.com.agree.dopomapping.valueobjectmapping.serializemapping.application.service.StudentService;
import cn.com.agree.dopomapping.valueobjectmapping.serializemapping.application.dto.StudentDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 值对象序列化 - 学生Controller
 */
@RestController
@RequestMapping("students")
public class StudentController {

    @Autowired
    private StudentService studentService;

    /**
     * 查询学生
     * @param id 主键
     * @return 结果
     */
    @GetMapping("/{id}")
    public StudentDTO findStudent(@PathVariable("id") String id) {
        return studentService.findStudent(id);
    }

    /**
     * 添加学生
     * @param studentDTO 学生信息
     */
    @PutMapping
    public StudentDTO addStudent(@RequestBody StudentDTO studentDTO) {
        return studentService.addStudent(studentDTO);
    }

}
