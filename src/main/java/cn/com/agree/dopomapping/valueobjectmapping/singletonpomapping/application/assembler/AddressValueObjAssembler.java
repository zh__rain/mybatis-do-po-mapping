package cn.com.agree.dopomapping.valueobjectmapping.singletonpomapping.application.assembler;


import cn.com.agree.dopomapping.valueobjectmapping.serializemapping.application.dto.CourseDTO;
import cn.com.agree.dopomapping.valueobjectmapping.singletonpomapping.domain.valueobject.AddressValueObj;
import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;

@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public interface AddressValueObjAssembler {

    CourseDTO toDTO(AddressValueObj user);

    AddressValueObj toEntity(CourseDTO userDTO);

}
