package cn.com.agree.dopomapping.valueobjectmapping.singletonpomapping.application.assembler;

import cn.com.agree.dopomapping.valueobjectmapping.singletonpomapping.application.dto.CustomerDTO;
import cn.com.agree.dopomapping.valueobjectmapping.singletonpomapping.domain.CustomerDO;
import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;

@Mapper(componentModel = MappingConstants.ComponentModel.SPRING, uses = {AddressValueObjAssembler.class})
public interface CustomerAssembler {

    CustomerDTO toDTO(CustomerDO user);

    CustomerDO toEntity(CustomerDTO userDTO);

}
