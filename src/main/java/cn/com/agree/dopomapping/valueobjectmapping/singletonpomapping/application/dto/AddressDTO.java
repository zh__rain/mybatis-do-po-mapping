package cn.com.agree.dopomapping.valueobjectmapping.singletonpomapping.application.dto;

import lombok.Data;

@Data
public class AddressDTO {

    /**
     * 省份
     */
    private String province;

    /**
     * 城市
     */
    private String city;

    /**
     * 区县
     */
    private String county;

    /**
     * 街道
     */
    private String street;

}
