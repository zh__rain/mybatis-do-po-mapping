package cn.com.agree.dopomapping.valueobjectmapping.singletonpomapping.application.dto;

import lombok.Data;

@Data
public class CustomerDTO {

    private String id;

    private String name;

    private Integer age;

    private AddressDTO address;

}
