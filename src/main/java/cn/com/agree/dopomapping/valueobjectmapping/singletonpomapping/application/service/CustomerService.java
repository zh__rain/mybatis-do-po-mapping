package cn.com.agree.dopomapping.valueobjectmapping.singletonpomapping.application.service;

import cn.com.agree.dopomapping.valueobjectmapping.singletonpomapping.application.assembler.CustomerAssembler;
import cn.com.agree.dopomapping.valueobjectmapping.singletonpomapping.application.dto.CustomerDTO;
import cn.com.agree.dopomapping.valueobjectmapping.singletonpomapping.domain.CustomerDO;
import cn.com.agree.dopomapping.valueobjectmapping.singletonpomapping.domain.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CustomerService {

    @Autowired
    private CustomerAssembler customerAssembler;

    @Autowired
    private CustomerRepository customerRepository;


    public CustomerDTO findCustomer(String id) {
        return customerAssembler.toDTO(customerRepository.findById(id));
    }

    public CustomerDTO addCustomer(CustomerDTO customerDTO) {
        CustomerDO customerDO = customerAssembler.toEntity(customerDTO);
        customerDO = customerRepository.save(customerDO);
        return customerAssembler.toDTO(customerDO);
    }
}
