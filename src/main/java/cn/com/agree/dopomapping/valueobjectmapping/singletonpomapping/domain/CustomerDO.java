package cn.com.agree.dopomapping.valueobjectmapping.singletonpomapping.domain;

import cn.com.agree.dopomapping.valueobjectmapping.singletonpomapping.domain.valueobject.AddressValueObj;
import lombok.Data;

@Data
public class CustomerDO {

    private Integer id;

    private String name;

    private Integer age;

    private AddressValueObj address;

}
