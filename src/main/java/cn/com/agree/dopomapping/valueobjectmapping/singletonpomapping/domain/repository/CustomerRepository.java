package cn.com.agree.dopomapping.valueobjectmapping.singletonpomapping.domain.repository;


import cn.com.agree.dopomapping.valueobjectmapping.singletonpomapping.domain.CustomerDO;

public interface CustomerRepository {

    CustomerDO findById(String id);

    CustomerDO save(CustomerDO user);

}
