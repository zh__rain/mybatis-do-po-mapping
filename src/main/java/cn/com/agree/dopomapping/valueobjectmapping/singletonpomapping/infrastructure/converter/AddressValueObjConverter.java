package cn.com.agree.dopomapping.valueobjectmapping.singletonpomapping.infrastructure.converter;

import cn.com.agree.dopomapping.valueobjectmapping.singletonpomapping.domain.valueobject.AddressValueObj;
import cn.com.agree.dopomapping.valueobjectmapping.singletonpomapping.infrastructure.po.AddressValueObjPO;
import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;

@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public interface AddressValueObjConverter {

    AddressValueObjPO toPO(AddressValueObj address, Integer customerId);

    AddressValueObj toEntity(AddressValueObjPO addressValueObjPO);

}
