package cn.com.agree.dopomapping.valueobjectmapping.singletonpomapping.infrastructure.converter;

import cn.com.agree.dopomapping.valueobjectmapping.singletonpomapping.domain.CustomerDO;
import cn.com.agree.dopomapping.valueobjectmapping.singletonpomapping.infrastructure.po.AddressValueObjPO;
import cn.com.agree.dopomapping.valueobjectmapping.singletonpomapping.infrastructure.po.CustomerPO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingConstants;

@Mapper(componentModel = MappingConstants.ComponentModel.SPRING, uses = AddressValueObjConverter.class)
public interface CustomerConverter {

    CustomerPO toPO(CustomerDO customerDO);

    @Mapping(target = "id", source = "customerPO.id")
    @Mapping(target = "address", source = "addressValueObjPO")
    CustomerDO toEntity(CustomerPO customerPO, AddressValueObjPO addressValueObjPO);

}
