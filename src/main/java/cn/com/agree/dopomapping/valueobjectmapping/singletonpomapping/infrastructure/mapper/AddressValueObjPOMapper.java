package cn.com.agree.dopomapping.valueobjectmapping.singletonpomapping.infrastructure.mapper;

import cn.com.agree.dopomapping.valueobjectmapping.singletonpomapping.infrastructure.po.AddressValueObjPO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface AddressValueObjPOMapper extends BaseMapper<AddressValueObjPO> {

    default AddressValueObjPO findByCustomerId(Integer customerId) {
        return selectOne(Wrappers.lambdaQuery(AddressValueObjPO.class)
                .eq(AddressValueObjPO::getCustomerId, customerId));
    }

}
