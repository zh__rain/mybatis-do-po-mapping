package cn.com.agree.dopomapping.valueobjectmapping.singletonpomapping.infrastructure.mapper;

import cn.com.agree.dopomapping.valueobjectmapping.singletonpomapping.infrastructure.po.CustomerPO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface CustomerPOMapper extends BaseMapper<CustomerPO> {
}
