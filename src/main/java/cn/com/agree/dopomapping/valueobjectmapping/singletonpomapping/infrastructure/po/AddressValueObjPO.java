package cn.com.agree.dopomapping.valueobjectmapping.singletonpomapping.infrastructure.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName("tb_address")
public class AddressValueObjPO {

    @TableId(type = IdType.AUTO)
    private Integer id;

    /**
     * 客户id
     */
    @TableField("customer_id")
    private Integer customerId;

    private String province;

    private String city;

    private String county;

    private String street;

}
