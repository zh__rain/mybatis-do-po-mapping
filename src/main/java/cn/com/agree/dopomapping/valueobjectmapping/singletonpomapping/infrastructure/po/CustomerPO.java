package cn.com.agree.dopomapping.valueobjectmapping.singletonpomapping.infrastructure.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName("tb_customer")
public class CustomerPO {

    @TableId(type = IdType.AUTO)
    private Integer id;

    private String name;

    private Integer age;

}
