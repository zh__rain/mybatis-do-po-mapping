package cn.com.agree.dopomapping.valueobjectmapping.singletonpomapping.interfaces.http;


import cn.com.agree.dopomapping.valueobjectmapping.singletonpomapping.application.dto.CustomerDTO;
import cn.com.agree.dopomapping.valueobjectmapping.singletonpomapping.application.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 客户Controller
 */
@RestController
@RequestMapping("customers")
public class CustomerController {

    @Autowired
    private CustomerService customerService;

    /**
     * 查询客户信息
     * @param id 主键
     * @return 结果
     */
    @GetMapping("/{id}")
    public CustomerDTO findUser(@PathVariable("id") String id) {
        return customerService.findCustomer(id);
    }

    /**
     * 添加客户
     * @param customerDTO 客户信息
     * @return 结果
     */
    @PutMapping
    public CustomerDTO addCustomer(@RequestBody CustomerDTO customerDTO) {
        return customerService.addCustomer(customerDTO);
    }

}
